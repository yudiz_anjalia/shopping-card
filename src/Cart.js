import React from "react";

const Cart = (props) =>{
    //  const {image,title,description,price,click} = props.values
      return(
          <div className="box">
              <div className='content'>
                  <h5>{props.items.title}</h5>
                  <p>{props.items.description}</p>
              </div>
              <img src={props.items.image}></img>
              
              <button onClick={() => props.click(props.items)}>Remove Item</button>
          </div>
      )
  
  }

export default Cart;