import React, { useState, useEffect } from 'react'
import Product from './Product'
import Cart from './Cart';
import './App.css'
import Nav from './Nav'
import './App.css';


const App = () => {
  const [cart, setCart] = useState([])
  const [products, setProducts] = useState([])
  const [toggle, setToggle] = useState('home')

  const getProduct = async () => {
    const response = await fetch("https://fakestoreapi.com/products");
    const product = await response.json()
    setProducts(product)
    console.log(product);
  }
  useEffect(() => {
    getProduct();
  }, [])

  const addCart = (items) => {
    console.log('in cart');
    setCart([...cart, { ...items }]);
  }

  const removeCart = (itemsRemove) => {
    setCart(cart.filter(items => items !== itemsRemove))
  }


  return (
    <div className="App">
      <Nav/>
      <button onClick={() => setToggle("cart")} > Cart {cart.length}</button>
      <button onClick={() => setToggle("home")}>Back to Item List</button>
      <div className="products">
        {toggle === "cart" ?
          <> {
            cart.map(items => {
              console.log(items);
              return (
                <Cart key={items.id} items={items} click={removeCart} />
              )
            })
          }</>
          : <>
            {products.map(items => {
              return (
                <Product key={items.id} items={items} click={addCart} />
              )
            })}
          </>}
      </div>
    </div>
  );
}

export default App;






