import React, { useState, useEffect } from 'react'
import './Nav.css'
import Cart from './Cart';
import Product from './Product';



const Nav = () => {
    const [cart, setCart] = useState([])
    const [products, setProducts] = useState([])
    const [toggle, setToggle] = useState('home')

    const addCart = (items) => {
        console.log('in cart');
        setCart([...cart, { ...items }]);
      }
    
      const removeCart = (itemsRemove) => {
        setCart(cart.filter(items => items !== itemsRemove))
      }
    return(
        <nav>
            <div className="nav__left">Shopping Store</div>
            <div className="nav__middle">
            <div className="input__wrapper">
                <input type="text"/>

            </div>
            </div>
            <div className="nav__right">
                <div className="cart__icon">
                <button onClick={() => setToggle("cart")}>Cart {cart.length}</button>
                <button onClick={() => setToggle("home")}>Back to Item List</button>   
                <div className="products">
        {toggle === "cart" ?
          <> {
            cart.map(items => {
              console.log(items);
              return (
                <Cart key={items.id} items={items} click={removeCart} />
              )
            })
          }</>
          : <>
            {products.map(items => {
              return (
                <Product key={items.id} items={items} click={addCart} />
              )
            })}
          </>}
      </div>   
                </div>
            </div>
            
        </nav>
    );
};

export default Nav;