import React from "react";

const Product = (props) =>{
  //  const {image,title,description,price,click} = props.values
    return(
        <div className="box">
            <div className='content'>
                <h5>{props.items.title}</h5>
                <p>{props.items.description}</p>
            </div>
            <img src={props.items.image}></img>
            <h4>{`Price: ${props.items.price}`}</h4>


            <button onClick={() => props.click(props.items)}>Add to cart</button>
        </div>
    )

}
export default Product;